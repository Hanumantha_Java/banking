package com.banking.BankingDemo;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.banking.dto.BenificiaryRequestall;
import com.banking.dto.BenificiryDto;
import com.banking.dto.LoginDto;
import com.banking.dto.LoginRequest;
import com.banking.dto.MoneyTranferRequest;
import com.banking.dto.TransferDto;
import com.banking.entity.Account;
import com.banking.entity.Beneficiary;
import com.banking.entity.Customer;
import com.banking.entity.Transactions;
import com.banking.exceptions.BeneficiaryExistException;
import com.banking.exceptions.BeneficiaryNotFoundException;
import com.banking.exceptions.BenificiaryAccountNotFoundException;
import com.banking.exceptions.InsufficientBalanceException;
import com.banking.exceptions.StartDateException;
import com.banking.repository.AccountRepository;
import com.banking.repository.BeneficiaryRepository;
import com.banking.repository.CustomerRepository;
import com.banking.repository.TransactionsRepository;
import com.banking.services.BankServices;



//@RunWith(MockitoJUnitRunner.Silent.class)

@ExtendWith(SpringExtension.class)
//@SpringBootTest
public class BankingDemoApplicationTests {

	
	@InjectMocks
	BankServices transactionService;
	@Mock
	BankServices transactionServiceMock;

	@Mock
	CustomerRepository customerRepository;
	@Mock
	AccountRepository accountRepository;
	@Mock
	TransactionsRepository transactionsRepository;
	@Mock
	BeneficiaryRepository beneficiaryRepository;

	Optional<LoginDto> responseDto;

	Customer customer;
	Transactions transaction;
	Account account;
	MoneyTranferRequest transactionRequestDto;
	Beneficiary beneficiary;
	Account account2;
	Account account3;
	Set<Beneficiary> beneficiaryList;
	Set<Transactions> transactionList;

	@BeforeEach
	public void before() {

		transactionList = new HashSet<Transactions>();
		transaction = new Transactions();
		transaction.setAccount(account);
		transaction.setFromaccount("11");
		transaction.setToaccount("22");
		transaction.setTransamount(new BigDecimal(1000));
		transaction.setStatus("debit");
		transaction.setClosingamount(new BigDecimal(10000));
		transactionList.add(transaction);

		beneficiary = new Beneficiary();
		beneficiary.setBenificiaryaccount(2);
		beneficiary.setName("Jack");
		beneficiaryList = new HashSet<Beneficiary>();
		beneficiaryList.add(beneficiary);

		account = new Account();
		account.setAccountnumber(11);
		account.setAmount(new BigDecimal(10000));
		account.setBranch("Darsi");
		account.setCreatetime((LocalDateTime.now()));
		account.setBeneficiary(beneficiaryList);
		account.setIfsc("ING123");
		account.setTransactions((transactionList));

		account2 = new Account();
		account2.setAccountnumber(22);
		account2.setAmount(new BigDecimal(20000));
		account2.setBranch("Darsi");
		account2.setCreatetime(LocalDateTime.now());
		account2.setIfsc("IITH");

		account3 = new Account();
		account3.setAccountnumber(33);
		account3.setAmount(new BigDecimal(10000));
		account3.setBranch("Ecity");
		account3.setCreatetime(LocalDateTime.now());

		customer = new Customer();
		customer.setCustomerId(1);
		customer.setName("hanumantha");
		customer.setPhonenumber("9966170145");
		customer.setMail("hanumantha@gmail.com");
		customer.setPassword("1234567");
		customer.setAddress("bangalore");
		}

	@Test
	public void testloginPositive() {
		LoginRequest loginRequestDto = new LoginRequest();
		loginRequestDto.setEmail("hanumantha@gmail.com");
		loginRequestDto.setPassword("1234567");
	//Mockito.when(customerRepository.save(customer)).thenReturn(customer);
        Mockito.when(customerRepository.existsByPasswordAndMail(loginRequestDto.getPassword(),loginRequestDto.getEmail())).thenReturn(true);
		ResponseEntity<LoginDto> responseDto= transactionService.loginrequest(loginRequestDto);
		//System.out.println(Constants.SUCCESS_CODE+":"+responseDto.getStatusCode());
		assertEquals(HttpStatus.OK, responseDto.getStatusCode());
	}

	@Test
	public void testloginNegative() {
		LoginRequest loginRequestDto = new LoginRequest();
		loginRequestDto.setEmail("anumantha@gmail.com");
		loginRequestDto.setPassword("123456");
		Mockito.when(customerRepository.existsByPasswordAndMail(loginRequestDto.getPassword(),
				loginRequestDto.getEmail())).thenReturn(false);
		ResponseEntity<LoginDto> responseDto= transactionService.loginrequest(loginRequestDto);
		assertEquals(HttpStatus.UNAUTHORIZED, responseDto.getStatusCode());
	}
	
	
	
	@Test
	public void TestAddBeneficiary() throws BeneficiaryNotFoundException, BeneficiaryExistException {
		
		BenificiaryRequestall beneficiaryRequestDto = new BenificiaryRequestall();
		beneficiaryRequestDto.setAccountid(11);
		beneficiaryRequestDto.setBenificiaryaccount(22);
		beneficiaryRequestDto.setName("Hanuman");
		beneficiaryRequestDto.setIfsc("IITH");
		beneficiaryRequestDto.setBranch("Darsi");
		Mockito.when(accountRepository.findByAccountnumber(beneficiaryRequestDto.getBenificiaryaccount()))
				.thenReturn(Optional.of(account2));
		//Mockito.when(accountRepository.save(account2)).thenReturn(account2);
		Mockito.when(accountRepository.existsByAccountnumber(beneficiaryRequestDto.getBenificiaryaccount())).thenReturn(true);
		beneficiary.setAccount(account2);
		Mockito.when(beneficiaryRepository.save(beneficiary)).thenReturn(beneficiary);

		ResponseEntity<BenificiryDto> responseDto = transactionService.addbenificiries(beneficiaryRequestDto);
		assertEquals(HttpStatus.CREATED, responseDto.getStatusCode());

	}

	
	
	@Test
	public void TestAddBeneficiaryNegative2() {
		BenificiaryRequestall beneficiaryRequestDto = new BenificiaryRequestall();
		beneficiaryRequestDto.setAccountid(11);
		beneficiaryRequestDto.setBenificiaryaccount(22);
		beneficiaryRequestDto.setName("Jack");
		beneficiaryRequestDto.setIfsc("ING456");
		beneficiaryRequestDto.setBranch("IITH");
		beneficiary.setAccount(account3);
		Mockito.when(beneficiaryRepository.save(beneficiary)).thenReturn(beneficiary);
		when(beneficiaryRepository.existsByBenificiaryaccount(beneficiaryRequestDto.getBenificiaryaccount()))
				.thenReturn(true);
		assertThrows(BeneficiaryExistException.class, ()->transactionService.addbenificiries(beneficiaryRequestDto));

}

	@Test
	public void transactionTest() throws InsufficientBalanceException, BenificiaryAccountNotFoundException {

		beneficiary = new Beneficiary();
		beneficiary.setAccount(account);
		beneficiary.setBenificiaryaccount(2);
		beneficiary.setName("Jack");

		transactionRequestDto = new MoneyTranferRequest();
		transactionRequestDto.setSouceaccount("11");
		transactionRequestDto.setDestiaccount("2");
		transactionRequestDto.setAmount(new BigDecimal(1000));
     	Mockito.when(accountRepository.findByAccountnumberAndAmountGreaterThanEqual(
				Integer.parseInt(transactionRequestDto.getSouceaccount()), transactionRequestDto.getAmount())).thenReturn(Optional.of(account));
		//Mockito.when(beneficiaryRepository.findByBenificiaryaccount(Integer.parseInt(transactionRequestDto.getDestiaccount()))).thenReturn(beneficiary);
		Mockito.when(accountRepository.findByAccountnumber(2)).thenReturn(Optional.of(account2));
		Mockito.when(beneficiaryRepository.existsByAccountAccountnumberAndBenificiaryaccount(Integer.parseInt(transactionRequestDto.getSouceaccount()),Integer.parseInt(transactionRequestDto.getDestiaccount()))).thenReturn(true);	
				Mockito.when(beneficiaryRepository.save(beneficiary)).thenReturn(beneficiary);

		Mockito.when(transactionsRepository.save(transaction)).thenReturn(transaction);
		ResponseEntity<TransferDto> responseDto = transactionService.transfermoney(transactionRequestDto);
		assertEquals(HttpStatus.OK, responseDto.getStatusCode());

	}

	@Test
	public void transactionTestNegative() throws InsufficientBalanceException, BenificiaryAccountNotFoundException {

		beneficiary = new Beneficiary();
		beneficiary.setAccount(account);
		beneficiary.setBenificiaryaccount(2);
		beneficiary.setName("Jack");

		transactionRequestDto = new MoneyTranferRequest();
		transactionRequestDto.setSouceaccount("11");
		transactionRequestDto.setDestiaccount("22");
		transactionRequestDto.setAmount(new BigDecimal(100000));

		Mockito.when(accountRepository
				.findByAccountnumberAndAmountGreaterThanEqual(Integer.parseInt(transactionRequestDto.getSouceaccount()),
						transactionRequestDto.getAmount())).thenReturn(Optional.of(account));
		Mockito.when(beneficiaryRepository.existsByAccountAccountnumberAndBenificiaryaccount(Integer.parseInt(transactionRequestDto.getSouceaccount()),Integer.parseInt(transactionRequestDto.getDestiaccount()))).thenReturn(true);	

		Mockito.when(beneficiaryRepository.findByBenificiaryaccount(Integer.parseInt(transactionRequestDto.getDestiaccount()))).thenReturn(beneficiary);
	//	Mockito.when(accountRepository.findByAccountnumber(2)).thenReturn(Optional.of(account2));
		//transactionService.transfermoney(transactionRequestDto);
assertThrows(BenificiaryAccountNotFoundException.class, ()->transactionService.transfermoney(transactionRequestDto));

	}

	@Test
	public void transactionTestNegative2() throws InsufficientBalanceException, BenificiaryAccountNotFoundException {

		beneficiary = new Beneficiary();
		beneficiary.setAccount(account3);
		beneficiary.setBenificiaryaccount(2);
		beneficiary.setName("Jack");

		transactionRequestDto = new MoneyTranferRequest();
		transactionRequestDto.setSouceaccount("11");
		transactionRequestDto.setDestiaccount("22");
		transactionRequestDto.setAmount(new BigDecimal(1000));

		Mockito.when(accountRepository.findByAccountnumberAndAmountGreaterThanEqual(
				Integer.parseInt(transactionRequestDto.getSouceaccount()), transactionRequestDto.getAmount()))
				.thenReturn(Optional.of(account));
		Mockito.when(
				beneficiaryRepository.findByBenificiaryaccount(Integer.parseInt(transactionRequestDto.getDestiaccount())))
				.thenReturn(beneficiary);
		Mockito.when(accountRepository.findByAccountnumber(22)).thenReturn(Optional.of(account2));

		Mockito.when(transactionsRepository.save(transaction)).thenReturn(transaction);
		assertThrows(BenificiaryAccountNotFoundException.class, ()->transactionService.transfermoney(transactionRequestDto));

	}
	
	@Test
	public void transactionHistoryTest() {
		String accountNumber = "11";
		String fromDate = "2019-07-02";
		String toDate = "2019-08-02";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime timeone = LocalDateTime.parse(fromDate + " 00:00:01", formatter);
		LocalDateTime timetwo = LocalDateTime.parse(toDate + " 23:59:59", formatter);
		Mockito.when(transactionsRepository.findAllByFromaccountAndTimeBetween(accountNumber, timeone, timetwo)).thenReturn(new ArrayList<Transactions>(transactionList));
		assertEquals(1, transactionsRepository.findAllByFromaccountAndTimeBetween(accountNumber, timeone, timetwo).size());
	}
	@Test
	public void transactionHistoryNegativeoneTest() {
		String accountNumber = "11";
		String fromDate = "2019-07-02";
		String toDate = "2019-08-02";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime timeone = LocalDateTime.parse(fromDate + " 00:00:01", formatter);
		LocalDateTime timetwo = LocalDateTime.parse(toDate + " 23:59:59", formatter);
		ArrayList<Transactions> transactionList = new ArrayList<Transactions>();
		Mockito.when(transactionsRepository.findAllByFromaccountAndTimeBetween(accountNumber, timeone, timetwo)).thenReturn(transactionList);
		assertThrows(StartDateException.class, ()->transactionService.gettranshistory(accountNumber,fromDate, toDate));

	}
	
	@Test
	public void transactionHistoryTestNegative() throws StartDateException {
		String accountNumber = "11";
		String toDate = "2019-08-02";
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd");
		LocalDateTime timeone = LocalDateTime.now().plusMonths(2);
		LocalDateTime timetwo = LocalDateTime.parse(toDate + " 23:59:59", formatter);
		String fromDate = timeone.format(formatter1);
		Mockito.when(transactionsRepository.findAllByFromaccountAndTimeBetween(accountNumber, timeone, timetwo)).thenReturn(new ArrayList<Transactions>(transactionList));
		//assertNotNull(transactionService.gettranshistory(accountNumber,fromDate, toDate));
		assertThrows(StartDateException.class, ()->transactionService.gettranshistory(accountNumber,fromDate, toDate));

	}

}
