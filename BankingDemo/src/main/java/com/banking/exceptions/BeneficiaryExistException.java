package com.banking.exceptions;

public class BeneficiaryExistException  extends Exception {
	private static final long serialVersionUID = -9079454849611061074L;

	public BeneficiaryExistException() {
		super();
	}

	public BeneficiaryExistException(final String message) {
		super(message);
	}


}

