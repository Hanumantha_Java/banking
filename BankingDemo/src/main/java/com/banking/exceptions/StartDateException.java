package com.banking.exceptions;

public class StartDateException extends Exception {

	private static final long serialVersionUID = -90454849611066574L;

	public StartDateException() {
		super();
	}

	public StartDateException(final String message) {
		super(message);
	}
}