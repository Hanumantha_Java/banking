package com.banking.exceptions;

import java.util.List;


public class ExceptionResponse {

	private String errorMessage;
	private List<String> details;

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(final String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public List<String> geDetails() {
		return details;
	}

	public void setDetails(final List<String> details) {
		this.details = details;
	}
}
