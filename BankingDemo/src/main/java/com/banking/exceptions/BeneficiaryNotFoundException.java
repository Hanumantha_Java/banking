package com.banking.exceptions;

public class BeneficiaryNotFoundException extends Exception {
	private static final long serialVersionUID = -9079454849611061074L;

	public BeneficiaryNotFoundException() {
		super();
	}

	public BeneficiaryNotFoundException(final String message) {
		super(message);
	}


}
