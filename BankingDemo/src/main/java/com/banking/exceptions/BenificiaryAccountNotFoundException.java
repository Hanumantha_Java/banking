package com.banking.exceptions;

public class BenificiaryAccountNotFoundException extends Exception {

	private static final long serialVersionUID = -9045749611066574L;

	public BenificiaryAccountNotFoundException() {
		super();
	}

	public BenificiaryAccountNotFoundException(final String message) {
		super(message);
	}
}