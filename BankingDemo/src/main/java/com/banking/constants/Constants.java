package com.banking.constants;

public class Constants {
	
	public static final String BENEFICIARY_NOTFOUND="account not added as beneficiary,Please add to beneficiary";

	public static final Integer SUCCESS_CODE = 200;
	public static final String TRANSACTION_SUCCESSFULL = "Transaction Successsfull";
	public static final String ADD_BENEFICIARY_SUCCESSFUL = "Beneficiary added successfully";
	public static final String LOGIN_SUCCESSFUL = "Logged in successfully";
			
	
	public static final Integer ACCOUNT_DOES_NOT_EXIST_CODE = 600;
	public static final String ACCOUNT_DOES_NOT_EXIST = "No account with the given account number exists";

	public static final Integer INSUFFICIENT_BALANCE_EXCEPTION_CODE = 601;
	public static final String INSUFFICIENT_BALANCE_EXCEPTION = "Sorry! Insufficient Balance to make a transaction";
	
	public static final Integer BENEFICIARY_ALREADY_EXISTS_CODE = 602;
	public static final String BENEFICIARY_ALREADY_EXISTS = "Beneficiary exists with the same account number";
	
	public static final Integer INVALID_CREDENTIALS_CODE = 603;
	public static final String INVALID_CREDENTIALS = "Invalid Username or Password";
	
	public static final Integer NEED_TO_LOGIN_FIRST_CODE = 604;
	public static final String NEED_TO_LOGIN_FIRST = "Please Login first to continue Banking Services";
	
	public static final Integer FUTURE_DATE_NOT_ALLOWED_CODE = 605;
	public static final String FUTURE_DATE_NOT_ALLOWED = "Start Date cannot be future date";
	
	public static final Integer BENEFICIARY_NOT_FOUND_CODE = 606;
	public static final String BENEFICIARY_NOT_FOUND = "No Beneficiary exists with this account Number";
	
	public static final Integer NO_RECORD_FOUND_CODE = 607;
	public static final String NO_RECORD_FOUND = "List is empty";

	private Constants() 
	
	{
    throw new IllegalStateException("Constants class");
			
	}
	}
