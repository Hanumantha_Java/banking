	package com.banking.dto;

import java.math.BigDecimal;

public class MoneyTranferRequest {
	
	private BigDecimal amount;
	private String souceaccount;
	private String destiaccount;
	public BigDecimal getAmount() {
		return amount;
	}
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}
	public String getSouceaccount() {
		return souceaccount;
	}
	public void setSouceaccount(String souceaccount) {
		this.souceaccount = souceaccount;
	}
	public String getDestiaccount() {
		return destiaccount;
	}
	public void setDestiaccount(String destiaccount) {
		this.destiaccount = destiaccount;
	}
	public MoneyTranferRequest(BigDecimal amount, String souceaccount, String destiaccount) {
		super();
		this.amount = amount;
		this.souceaccount = souceaccount;
		this.destiaccount = destiaccount;
	}
	public MoneyTranferRequest() {
		super();

	}
	

}
