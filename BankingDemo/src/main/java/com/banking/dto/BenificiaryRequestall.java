package com.banking.dto;

public class BenificiaryRequestall {

	private int accountid;

	private int benificiaryaccount;

	private String ifsc;

	private String branch;
	private String name;
	public int getBenificiaryaccount() {
		return benificiaryaccount;
	}

	public void setBenificiaryaccount(int benificiaryaccount) {
		this.benificiaryaccount = benificiaryaccount;
	}


	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public BenificiaryRequestall() {
		super();

	}

	public BenificiaryRequestall(int accountid, int benificiaryaccount, String ifsc, String branch,String name) {
		super();
		this.accountid = accountid;
		this.benificiaryaccount = benificiaryaccount;
		this.ifsc = ifsc;
		this.branch = branch;
		this.name=name;
	}

	public int getAccountid() {
		return accountid;
	}

	public void setAccountid(int accountid) {
		this.accountid = accountid;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}



}
