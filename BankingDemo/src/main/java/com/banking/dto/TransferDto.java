package com.banking.dto;

public class TransferDto {
	
	private String message;
	private int statuscode;

	public int getStatuscode() {
		return statuscode;
	}

	public void setStatuscode(int i) {
		this.statuscode = i;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public TransferDto() 
	{
		// create instantiation
	}

	@Override
	public String toString() {
		return "TransferDto [message=" + message + ", statuscode=" + statuscode + "]";
	}

}
