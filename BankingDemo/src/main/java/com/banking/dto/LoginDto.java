package com.banking.dto;

public class LoginDto {
	
    private String message;
	private int statuscode;

	public int getStatuscode() {
		return statuscode;
	}

	public void setStatuscode(int statuscode) {
		this.statuscode = statuscode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

public LoginDto()
{
	// create instantiation	
}
@Override
public String toString() {
	return "LoginDto [message=" + message + ", statuscode=" + statuscode + "]";
}


}
