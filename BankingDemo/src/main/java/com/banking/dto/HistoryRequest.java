package com.banking.dto;

import java.time.LocalDateTime;

public class HistoryRequest {
	
	private String sourceaccount;
	private LocalDateTime starttime;
	private LocalDateTime endtime;
	public HistoryRequest() {
		super();

	}
	public HistoryRequest(String sourceaccount, LocalDateTime starttime, LocalDateTime endtime) {
		super();
		this.sourceaccount = sourceaccount;
		this.starttime = starttime;
		this.endtime = endtime;
	}
	public HistoryRequest(LocalDateTime endtime) {
		super();
		this.endtime = endtime;
	}
	public String getSourceaccount() {
		return sourceaccount;
	}
	public void setSourceaccount(String sourceaccount) {
		this.sourceaccount = sourceaccount;
	}
	public LocalDateTime getStarttime() {
		return starttime;
	}
	public void setStarttime(LocalDateTime starttime) {
		this.starttime = starttime;
	}
	public LocalDateTime getEndtime() {
		return endtime;
	}
	public void setEndtime(LocalDateTime endtime) {
		this.endtime = endtime;
	}
	

}
