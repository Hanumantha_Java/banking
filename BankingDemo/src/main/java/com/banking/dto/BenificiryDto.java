package com.banking.dto;

public class BenificiryDto {
	
	private String message;
	private int statuscode;

	public int getStatuscode() {
		return statuscode;
	}

	public void setStatuscode(int statuscode) {
		this.statuscode = statuscode;
	}

	public String getMessage() {
		return message;
	}

	@Override
	public String toString() {
		return "BenificiryDto [message=" + message + ", statuscode=" + statuscode + "]";
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public BenificiryDto() 
	{
		// create instantiation
	}
	

}
