package com.banking.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.banking.beneficiaryservice.BeneficiaryService;
import com.banking.customerservice.CustomerService;
import com.banking.dto.BenificiaryRequestall;
import com.banking.dto.BenificiryDto;
import com.banking.dto.LoginDto;
import com.banking.dto.LoginRequest;
import com.banking.dto.MoneyTranferRequest;
import com.banking.dto.TransferDto;
import com.banking.entity.Transactions;
import com.banking.exceptions.BeneficiaryExistException;
import com.banking.exceptions.BeneficiaryNotFoundException;
import com.banking.exceptions.BenificiaryAccountNotFoundException;
import com.banking.exceptions.InsufficientBalanceException;
import com.banking.exceptions.StartDateException;
import com.banking.services.BankServices;
import com.banking.transactionsservice.TransactionsService;
@RequestMapping("/bank")
@RestController
public class BankController {
	@Autowired
	BankServices bankservices;
	@Autowired
	TransactionsService transactionsService;
	@Autowired
	BeneficiaryService beneficiaryService;
	@Autowired
	CustomerService customerService;
	
	/*
	 *@param(email,password)
	 */
	@PostMapping("login")
	
	public ResponseEntity<LoginDto> login(@RequestBody LoginRequest loginrequest)
	
	{
		return customerService.loginrequest(loginrequest);
	}
	

	/*
	 *@param(amount,sourceaccount,destinationaccount)
	 */
	@PostMapping("moneylransfer")
	public ResponseEntity<TransferDto> moneytransfer(@RequestBody MoneyTranferRequest oneyTranferRequest) throws InsufficientBalanceException,BenificiaryAccountNotFoundException
	{
		return transactionsService.transfermoney(oneyTranferRequest);
	}
	
	/*
	 *@param(accountnumber,starttime,endtime)
	 */
	@GetMapping("gettransactionhistory")
	
	public List<Transactions> gettranshistory(@RequestParam(value="accountnumber") String accountnumber,@RequestParam(value="starttime") String starttime,@RequestParam(value="endtime") String endtime) throws StartDateException
	{
		return transactionsService.gettranshistory(accountnumber,starttime,endtime);
	}
	/*
	 *@param(accountnumber,beneficiaryaccount,ifsccode,branch,name)
	 */
	@PostMapping("addingneficiary")
	public ResponseEntity<BenificiryDto> addbenificiry(@RequestBody BenificiaryRequestall beneficiaryRequest) throws  BeneficiaryNotFoundException, BeneficiaryExistException
	{
		return beneficiaryService.addbenificiries(beneficiaryRequest);
	}

}
