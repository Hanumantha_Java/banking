package com.banking.customerserviceimpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.banking.dto.LoginDto;
import com.banking.dto.LoginRequest;
import com.banking.repository.CustomerRepository;

public class CustomerServiceImpl {
	@Autowired
	private CustomerRepository customerRepository;
	
	public ResponseEntity<LoginDto> loginrequest(LoginRequest loginrequest) {
		LoginDto loginDto = new LoginDto();

		if (customerRepository.existsByPasswordAndMail(loginrequest.getPassword(), loginrequest.getEmail())) {
			loginDto.setMessage("Successfully login");
			loginDto.setStatuscode(HttpStatus.OK.value());
			return new ResponseEntity<>(loginDto, HttpStatus.OK);
		}

		else {
			loginDto.setMessage("Login failed");
			loginDto.setStatuscode(HttpStatus.UNAUTHORIZED.value());
			return new ResponseEntity<>(loginDto, HttpStatus.UNAUTHORIZED);
		}
	}

}
