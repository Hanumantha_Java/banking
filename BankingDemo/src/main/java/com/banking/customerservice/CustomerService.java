package com.banking.customerservice;

import org.springframework.http.ResponseEntity;

import com.banking.dto.LoginDto;
import com.banking.dto.LoginRequest;

public interface CustomerService {
	public ResponseEntity<LoginDto> loginrequest(LoginRequest loginrequest);

}
