package com.banking.beneficiaryserviceimpl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.banking.dto.BenificiaryRequestall;
import com.banking.dto.BenificiryDto;
import com.banking.entity.Account;
import com.banking.entity.Beneficiary;
import com.banking.exceptions.BeneficiaryExistException;
import com.banking.exceptions.BeneficiaryNotFoundException;
import com.banking.repository.AccountRepository;
import com.banking.repository.BeneficiaryRepository;


public class BeneficiaryServiceImpl {
	
	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private BeneficiaryRepository beneficiaryRepository;

	
	public ResponseEntity<BenificiryDto> addbenificiries(BenificiaryRequestall beneficiaryRequest)
			throws BeneficiaryNotFoundException, BeneficiaryExistException {
		BenificiryDto benificiryDTO = new BenificiryDto();
		// check beneficiary is already exists or not.
		if (beneficiaryRepository.existsByBenificiaryaccount(beneficiaryRequest.getBenificiaryaccount())) {
			throw new BeneficiaryExistException("beneficiary already exist");
		}
		if ((accountRepository.existsByAccountnumber(beneficiaryRequest.getBenificiaryaccount()))) {
			Optional<Account> mainaccount = accountRepository
					.findByAccountnumber(beneficiaryRequest.getBenificiaryaccount());
			if(mainaccount.isPresent()) {
				
			if ((mainaccount.get().getBranch().equalsIgnoreCase(beneficiaryRequest.getBranch()))&& (mainaccount.get().getIfsc().equalsIgnoreCase(beneficiaryRequest.getIfsc()))) {

				Beneficiary b = new Beneficiary();
				b.setBenificiaryaccount(beneficiaryRequest.getBenificiaryaccount());
				b.setBranch(beneficiaryRequest.getBranch());
				b.setIfsc(beneficiaryRequest.getIfsc());
				b.setName(beneficiaryRequest.getName());
				Account account = new Account();
				account.setAccountnumber(beneficiaryRequest.getAccountid());
				b.setAccount(account);
				beneficiaryRepository.save(b);
				benificiryDTO.setMessage("benfeciary added sussessfully");
				benificiryDTO.setStatuscode(HttpStatus.CREATED.value());
				return new ResponseEntity<>(benificiryDTO, HttpStatus.CREATED);
			}
			
			else
			{
				throw new BeneficiaryNotFoundException("Branch or ifsc code does't match");
			}
			}
			else {
				throw new BeneficiaryNotFoundException("Branch or ifsc code does't match");
			}
		}
		else {
			throw new BeneficiaryNotFoundException("beneficiary account does't exist in internal bank");
		}

	}


}
