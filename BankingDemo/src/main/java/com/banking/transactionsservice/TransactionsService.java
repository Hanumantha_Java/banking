package com.banking.transactionsservice;

import java.util.List;

import org.springframework.http.ResponseEntity;

import com.banking.dto.MoneyTranferRequest;
import com.banking.dto.TransferDto;
import com.banking.entity.Transactions;


public interface TransactionsService {
	
	public ResponseEntity<TransferDto> transfermoney(MoneyTranferRequest moneytransferrequest);
			
	public List<Transactions> gettranshistory(String accountnumber, String starttime, String endtime);
}
