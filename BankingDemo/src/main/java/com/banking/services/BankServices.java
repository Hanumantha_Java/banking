package com.banking.services;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.banking.constants.Constants;
import com.banking.dto.BenificiaryRequestall;
import com.banking.dto.BenificiryDto;
import com.banking.dto.LoginDto;
import com.banking.dto.LoginRequest;
import com.banking.dto.MoneyTranferRequest;
import com.banking.dto.TransferDto;
import com.banking.entity.Account;
import com.banking.entity.Beneficiary;
import com.banking.entity.Transactions;
import com.banking.exceptions.BeneficiaryExistException;
import com.banking.exceptions.BeneficiaryNotFoundException;
import com.banking.exceptions.BenificiaryAccountNotFoundException;
import com.banking.exceptions.InsufficientBalanceException;
import com.banking.exceptions.StartDateException;
import com.banking.repository.AccountRepository;
import com.banking.repository.BeneficiaryRepository;
import com.banking.repository.CustomerRepository;
import com.banking.repository.TransactionsRepository;

@Service
public class BankServices {

	@Autowired
	private AccountRepository accountRepository;
	@Autowired
	private BeneficiaryRepository beneficiaryRepository;
	@Autowired
	private TransactionsRepository transactionsRepository;
	@Autowired
	private CustomerRepository customerRepository;

	public ResponseEntity<TransferDto> transfermoney(MoneyTranferRequest moneytransferrequest)
			throws InsufficientBalanceException, BenificiaryAccountNotFoundException {
		TransferDto transferdto = new TransferDto();

		if (!(beneficiaryRepository.existsByAccountAccountnumberAndBenificiaryaccount(Integer.parseInt(moneytransferrequest.getSouceaccount()),Integer.parseInt(moneytransferrequest.getDestiaccount()))))
		{
			
			throw new BenificiaryAccountNotFoundException(Constants.BENEFICIARY_NOTFOUND);
		}
	
	else {

			BigDecimal amount = moneytransferrequest.getAmount();
//check balance amount if amount is less than reqursted amount then send notifcation to user as insufficient balance
			Account source = accountRepository
					.findByAccountnumberAndAmountGreaterThanEqual((Integer.parseInt(moneytransferrequest.getSouceaccount())), amount)
					.orElseThrow(() -> new InsufficientBalanceException("insufficient balance"));
			Optional<Account> destination = accountRepository.findByAccountnumber((Integer.parseInt(moneytransferrequest.getDestiaccount())));

			// add money to destination account and debit from souce
			source.setAmount(source.getAmount().subtract(amount));
			if(destination.isPresent())
			{
			destination.get().setAmount(destination.get().getAmount().add(amount));
			}
			else
			{
				throw new BenificiaryAccountNotFoundException(Constants.BENEFICIARY_NOTFOUND);

			}
			// add both transaction debit and credit accounts transactions
			Set<Transactions> transaction = new HashSet<>();
			Transactions transone = new Transactions();
			Transactions transtwo = new Transactions();
			transone.setAccount(source);
			transone.setTransamount(amount);
			transone.setStatus("debited");
			transone.setClosingamount(source.getAmount());
			transone.setTime(LocalDateTime.now());
			transone.setFromaccount(String.valueOf(source.getAccountnumber()));
			transone.setToaccount(String.valueOf(destination.get().getAccountnumber()));
			transtwo.setAccount(destination.get());
			transtwo.setTransamount(amount);
			transtwo.setStatus("Credited");
			transtwo.setClosingamount(destination.get().getAmount());
			transtwo.setTime(LocalDateTime.now());
			transtwo.setFromaccount(String.valueOf(destination.get().getAccountnumber()));
			transtwo.setToaccount(String.valueOf(source.getAccountnumber()));
			transaction.add(transone);
			transaction.add(transtwo);
			transactionsRepository.saveAll(transaction);
			transferdto.setStatuscode(HttpStatus.OK.value());
			transferdto.setMessage("transaction is successfull,debited amount is:" + amount);
			return new ResponseEntity<>(transferdto, HttpStatus.OK);
		}
	}

	public List<Transactions> gettranshistory(String accountnumber, String starttime, String endtime)
			throws StartDateException

	{
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime timeone = LocalDateTime.parse(starttime + " 00:00:01", formatter);
		LocalDateTime timetwo = LocalDateTime.parse(endtime + " 23:59:01", formatter);
		if (timeone.toLocalDate().isAfter(LocalDate.now())) {
			throw new StartDateException("Start date should not be future date");
		}

		/*
		 * select * from Tansactons where fromaccount=accountnumber and time between
		 * timeone and timetwo
		 */
		List<Transactions> trans = transactionsRepository.findAllByFromaccountAndTimeBetween(accountnumber, timeone,
				timetwo);
		if (trans.isEmpty()) {
			throw new StartDateException("No transactions history");
		}
		return trans;
	}

	public ResponseEntity<LoginDto> loginrequest(LoginRequest loginrequest) {
		LoginDto loginDto = new LoginDto();

		if (customerRepository.existsByPasswordAndMail(loginrequest.getPassword(), loginrequest.getEmail())) {
			loginDto.setMessage("Successfully login");
			loginDto.setStatuscode(HttpStatus.OK.value());
			return new ResponseEntity<>(loginDto, HttpStatus.OK);
		}

		else {
			loginDto.setMessage("Login failed");
			loginDto.setStatuscode(HttpStatus.UNAUTHORIZED.value());
			return new ResponseEntity<>(loginDto, HttpStatus.UNAUTHORIZED);
		}
	}

	public ResponseEntity<BenificiryDto> addbenificiries(BenificiaryRequestall beneficiaryRequest)
			throws BeneficiaryNotFoundException, BeneficiaryExistException {
		BenificiryDto benificiryDTO = new BenificiryDto();
		// check beneficiary is already exists or not.
		if (beneficiaryRepository.existsByBenificiaryaccount(beneficiaryRequest.getBenificiaryaccount())) {
			throw new BeneficiaryExistException("beneficiary already exist");
		}
		if ((accountRepository.existsByAccountnumber(beneficiaryRequest.getBenificiaryaccount()))) {
			Optional<Account> mainaccount = accountRepository
					.findByAccountnumber(beneficiaryRequest.getBenificiaryaccount());
			if(mainaccount.isPresent()) {
				
			if ((mainaccount.get().getBranch().equalsIgnoreCase(beneficiaryRequest.getBranch()))&& (mainaccount.get().getIfsc().equalsIgnoreCase(beneficiaryRequest.getIfsc()))) {

				Beneficiary b = new Beneficiary();
				b.setBenificiaryaccount(beneficiaryRequest.getBenificiaryaccount());
				b.setBranch(beneficiaryRequest.getBranch());
				b.setIfsc(beneficiaryRequest.getIfsc());
				b.setName(beneficiaryRequest.getName());
				Account account = new Account();
				account.setAccountnumber(beneficiaryRequest.getAccountid());
				b.setAccount(account);
				beneficiaryRepository.save(b);
				benificiryDTO.setMessage("benfeciary added sussessfully");
				benificiryDTO.setStatuscode(HttpStatus.CREATED.value());
				return new ResponseEntity<>(benificiryDTO, HttpStatus.CREATED);
			}
			
			else
			{
				throw new BeneficiaryNotFoundException("Branch or ifsc code does't match");
			}
			}
			else {
				throw new BeneficiaryNotFoundException("Branch or ifsc code does't match");
			}
		}
		else {
			throw new BeneficiaryNotFoundException("beneficiary account does't exist in internal bank");
		}

	}

}
