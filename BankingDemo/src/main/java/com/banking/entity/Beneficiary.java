package com.banking.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
public class Beneficiary {

	@Id
	private int benificiaryaccount;

	private String name;

	private String ifsc;

	private String branch;

	public int getBenificiaryaccount() {
		return benificiaryaccount;
	}

	public void setBenificiaryaccount(int benificiaryid) {
		this.benificiaryaccount = benificiaryid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public Beneficiary(int benificiaryid, String name, String ifsc, String branch) {
		super();
		this.benificiaryaccount = benificiaryid;
		this.name = name;
		this.ifsc = ifsc;
		this.branch = branch;
	}

	public Beneficiary() {
		super();

	}
	@ManyToOne
	@JoinColumn(name = "fk_account")
	Account account;

	public Account getAccount() {
		return account;
	}

	public Beneficiary(Account account) {
		super();
		this.account = account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

}
