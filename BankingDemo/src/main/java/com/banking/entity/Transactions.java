package com.banking.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity

public class Transactions {

	@Id
	@GeneratedValue
	private int transid;
	@Column(nullable = false, unique = true)
	private String fromaccount;
	@Column(nullable = false, unique = true)
	private String toaccount;
	private String status;
	private LocalDateTime time;
	private BigDecimal transamount;
	private BigDecimal closingamount;

	public int getTransid() {
		return transid;
	}

	public void setTransid(int transid) {
		this.transid = transid;
	}

	public String getFromaccount() {
		return fromaccount;
	}

	public void setFromaccount(String fromaccount) {
		this.fromaccount = fromaccount;
	}

	public String getToaccount() {
		return toaccount;
	}

	public void setToaccount(String toaccount) {
		this.toaccount = toaccount;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public LocalDateTime getTime() {
		return time;
	}

	public void setTime(LocalDateTime time) {
		this.time = time;
	}

	public BigDecimal getTransamount() {
		return transamount;
	}

	public void setTransamount(BigDecimal transamount) {
		this.transamount = transamount;
	}

	public BigDecimal getClosingamount() {
		return closingamount;
	}

	public void setClosingamount(BigDecimal closingamount) {
		this.closingamount = closingamount;
	}

	public Transactions(int transid, String fromaccount, String toaccount, String status, LocalDateTime time, BigDecimal transamount,
			BigDecimal closingamount) {
		super();
		this.transid = transid;
		this.fromaccount = fromaccount;
		this.toaccount = toaccount;
		this.status = status;
		this.time = time;
		this.transamount = transamount;
		this.closingamount = closingamount;
	}

	public Transactions() {
		super();

	}

	@ManyToOne
	@JoinColumn(name = "fk_trans")
	private Account account;
	@JsonIgnore
	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Transactions(Account account) {
		super();
		this.account = account;
	}


}
