package com.banking.entity;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;

@Entity
public class Customer {

	@Id
	@GeneratedValue
	private int customerId;
	private String name;
	@Column(nullable = false, unique = true)
	private String mail;
	private String phonenumber;
	private String address;
	private String password;

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getCustomerId() {
		return customerId;
	}

	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(String phonenumber) {
		this.phonenumber = phonenumber;
	}

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "fk_account")
	Account account;

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public Customer(int customerId, String name, String mail, String phonenumber, String address, String password) {
		super();
		this.customerId = customerId;
		this.name = name;
		this.mail = mail;
		this.phonenumber = phonenumber;
		this.address = address;
		this.password = password;
	}

	public Customer() {
		super();

	}

	public Customer(Account account) {
		super();
		this.account = account;
	}

}
