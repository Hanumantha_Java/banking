package com.banking.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Account {

	@Id
	@GeneratedValue
	private int accountnumber;
	private String ifsc;
	private String branch;
	private BigDecimal amount;
	private LocalDateTime createtime;

	public LocalDateTime getCreatetime() {
		return createtime;
	}

	public void setCreatetime(LocalDateTime createtime) {
		this.createtime = createtime;
	}

	public int getAccountnumber() {
		return accountnumber;
	}

	public void setAccountnumber(int accountnumber) {
		this.accountnumber = accountnumber;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getBranch() {
		return branch;
	}

	public void setBranch(String branch) {
		this.branch = branch;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	public Account(int accountnumber, String ifsc, String branch, BigDecimal amount, LocalDateTime time) {
		super();
		this.accountnumber = accountnumber;
		this.ifsc = ifsc;
		this.branch = branch;
		this.amount = amount;
		this.createtime = time;
	}

	@OneToMany(cascade = CascadeType.ALL,mappedBy = "account")
    private Set<Beneficiary> beneficiary;

	@OneToMany(cascade = CascadeType.ALL, mappedBy = "account")
	private Set<Transactions> transactions;

	public Set<Beneficiary> getBeneficiary() {
		return beneficiary;
	}

	public void setBeneficiary(Set<Beneficiary> beneficiary) {
		this.beneficiary = beneficiary;
	}

	public Set<Transactions> getTransactions() {
		return transactions;
	}

	public void setTransactions(Set<Transactions> transactions) {
		this.transactions = transactions;
	}

	public Account() {
		super();

	}

}
