package com.banking.repository;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.banking.entity.Account;

public interface AccountRepository extends JpaRepository<Account, Integer>{

	Optional<Account> findByAccountnumber(int accountnumber);

	Optional<Account> findByAccountnumberAndAmountGreaterThanEqual(int i, BigDecimal amount);

	boolean existsByAccountnumber(int benificiaryid);

}
