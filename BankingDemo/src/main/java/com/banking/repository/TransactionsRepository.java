package com.banking.repository;

import java.time.LocalDateTime;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.banking.entity.Transactions;

public interface TransactionsRepository  extends JpaRepository<Transactions, Integer>{

	List<Transactions> findAllByFromaccountAndTimeBetween(String string, LocalDateTime timeone, LocalDateTime timetwo);

}
